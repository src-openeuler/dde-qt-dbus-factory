%global soname dframeworkdbus

Name:           dde-qt-dbus-factory
Version:        5.5.22
Release:        1%{?fedora:%dist}
Summary:        A repository stores auto-generated Qt5 dbus code
# The entire source code is GPLv3+ except
# libdframeworkdbus/qtdbusextended/ which is LGPLv2+
License:        GPLv3+ and LGPLv2+
URL:            https://github.com/linuxdeepin/dde-qt-dbus-factory
Source0:        %{name}-%{version}.tar.gz

BuildRequires:  python3
BuildRequires:  pkgconfig(gl)
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5DBus)
BuildRequires:  pkgconfig(Qt5Gui)
BuildRequires:  qt5-qtbase-private-devel
%{?_qt5:Requires: %{_qt5}%{?_isa} = %{_qt5_version}}

%description
A repository stores auto-generated Qt5 dbus code.

%package devel
Summary:        Development package for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       cmake-filesystem

%description devel
Header files and libraries for %{name}.

%prep
%autosetup -p1 -n %{name}-%{version}
sed -i "s/env python$/env python3/g" libdframeworkdbus/generate_code.py

%build
%qmake_qt5 LIB_INSTALL_DIR=%{_libdir}
%make_build

%install
%make_install INSTALL_ROOT=%{buildroot}

%files
%doc README.md CHANGELOG.md technology-overview.md
%license LICENSE
%{_libdir}/lib%{soname}.so.2*

%files devel
%{_includedir}/lib%{soname}-2.0/
%{_libdir}/pkgconfig/%{soname}.pc
%{_libdir}/lib%{soname}.so
%{_libdir}/cmake/DFrameworkdbus/

%changelog
* Mon Jul 31 2023 leeffo <liweiganga@uniontech.com> - 5.5.22-1
- upgrade to version 5.5.22

* Mon Mar 27 2023 liweiganga <liweiganga@uniontech.com> - 5.4.21-1
- update: update to 5.4.21

* Mon Jul 18 2022 konglidong <konglidong@uniontech.com> - 5.4.5-1
- Update to 5.4.5

* Sat Jan 29 2022 liweigang <liweiganga@uniontech.com> - 5.3.0.19-2
- fix build error

* Thu Jul 08 2021 weidong <weidong@uniontech.com> - 5.3.0.19-1
- Update 5.3.0.19

* Thu Jul 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.2.0.0-3
- Package init
